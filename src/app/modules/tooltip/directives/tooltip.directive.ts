import {
  Directive,
  HostListener,
  Input,
  ViewContainerRef,
  ComponentFactoryResolver,
  ElementRef,
} from '@angular/core';
import { TooltipComponent } from '../components/tooltip/tooltip.component';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[tooltip]',
})
export class TooltipDirective {
  @Input() tooltip = '';
  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const clickedInside = this.hostElementRef.nativeElement.contains(
      targetElement
    );
    if (clickedInside) {
      this.createTooltip();
    } else {
      this.viewContainer.clear();
    }
  }
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(
    event: KeyboardEvent
  ) {
    this.viewContainer.clear();
  }
  constructor(
    private resolver: ComponentFactoryResolver,
    private viewContainer: ViewContainerRef,
    private hostElementRef: ElementRef,
  ) {}

  createTooltip(): void {
    const componentFactory = this.resolver.resolveComponentFactory(
      TooltipComponent
    );
    this.viewContainer.clear();
    const componentRef = this.viewContainer.createComponent(componentFactory);
    componentRef.instance.message = this.tooltip;
    // getting the component's HTML
    const element: HTMLElement = componentRef.location
      .nativeElement as HTMLElement;

    // adding styles
    setTimeout(() => {
      element.style.visibility = 'visible';
      if (
        this.hostElementRef.nativeElement.offsetTop > 50
      ) {
        element.style.top = `${
          this.hostElementRef.nativeElement.offsetTop -
          element.clientHeight -
          10
        }px`;
        element.style.left = `${
          this.hostElementRef.nativeElement.offsetLeft - 10
        }px`;
      } else {
        element.style.top = `${
          this.hostElementRef.nativeElement.offsetTop +
          this.hostElementRef.nativeElement.offsetHeight +
          10
        }px`;
        element.style.left = `${
          this.hostElementRef.nativeElement.offsetLeft + 10
        }px`;
      }
    }, 0);
  }
}
