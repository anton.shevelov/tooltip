import { Component, ViewChild, HostBinding } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @HostBinding('style.align-items')
  $alignItems = 'flex-start';
  @HostBinding('style.justify-content')
  $justifyContent = 'flex-start';

  justifyContent(align): void {
    this.$justifyContent = align;
  }
  alignItems(align): void {
    this.$alignItems = align;
  }
}
