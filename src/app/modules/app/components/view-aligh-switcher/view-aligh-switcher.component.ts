import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-view-aligh-switcher',
  templateUrl: './view-aligh-switcher.component.html',
  styleUrls: ['./view-aligh-switcher.component.scss'],
})
export class ViewAlighSwitcherComponent implements OnInit {
  @Output() justifyContent: EventEmitter<string> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  updateJustifyContent(align): void {
    this.justifyContent.emit(align);
  }
}
